MGF Test Spec


**PHP Developer Test**
Before inviting you to interview, we would like to get a good idea of your system design and software skills. 
This isn’t a pass or fail exercise; it’s more a discussion point.
Test summary
We are keen to know how you apply fundamental app/ systems design with back end OOP PHP to common tasks worked-on within the company as well as some basic front end jQuery. 
Data basics
At MGF we have three core types of user in our software systems (highlighted in bold):
(1) Engineers (ENG_): typically Design Engineers who create solutions for our clients 
(2) Internal (INT_): all other staff who aren’t Engineers e.g. software developers, admin, directors
(3) External (EXT_): any person who is not on the company payroll e.g. clients and potential customers
These three user types, as expected, all reside in individual MySQL tables and all have different fields/ properties and all have an individual ID. 
Throughout our systems we like to pass around a single “User” allowing us to carry out downstream tasks for all “Users” that take into account all their individual properties.
Test Protocol 
Although the solutions to these 4 tasks can be achieved relatively simply, we are looking for core OOP PHP principles that allow for further expansion and modularity; given the information above.
Please don’t use MySQL or any PHP framework.
(1) Create a basic GUI that calls and displays the contents from the following URL when a button is pressed
http://www.mgf.ltd.uk/software-test/api.php
Pass in POST variables {mgf=userData; apiKey=123455678qwertyui} to receive the data
You will receive text in JSON form which takes data from three tables {“Engineers”, “Internal” and “External”} where the key is the unique id from that table
Display formatted User data in a scrollable area and suitable readable format where each type of user is clearly highlighted 
The data should be displayed efficiently from a single View class

(2) Create a text-file-based cache such that if the above API is requested again, you get the data from the cache, rather than from the API. 
Make it obvious in the display that your data is getting called from a cache 
Have a method for resetting the cache
(3) Have a button on the GUI that converts and displays the raw data to and from different formats and displays them accordingly
(a) CSV
(b) XML
(c) JSON
(d) Allow for further conversions
(4) Have a button which outputs to the console log the average salary and average age (in years, months and days) for each user, if applicable.


