<?php

/**
 * @author Paul Riley 
 * MGL Test Project
 */

/**
 * Main controller
 */

class Controller
{
    private $model;

    public function __construct($model) {
        $this->model = $model;
    }
    
    function refresh(){
        var_dump('refreshing');
        $this->model->refreshData();
    }
    
    function averageAge()
    {
        $i=0;
        $age= 0;
   
        foreach ($this->model->data as $user){

            if (is_callable(array($user,'getAge'))) {
           
                $age += $user->getAge();
            
                $i++;
            }
        }
     
      
        
        $age = $age / $i;
  
        $years = floor($age / (365 * 60 * 60 * 24));
        $months = floor(($age - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($age - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

        echo (($years > 0) ? $years . ' Year' . ($years > 1 ? 's' : '') . ', ' : '') . 
             (($months > 0) ? $months . ' Month' . ($months > 1 ? 's' : '') . ', ' : '') . 
             (($days > 0) ? $days . ' Day' . ($days > 1 ? 's' : '') : '');
    }
    
     function averageSalary()
    {
        $i=0;
        $salary= 0;
   
        foreach ($this->model->data as $user){

            if (is_callable(array($user,'getSalary'))) {
           
                $salary += $user->getSalary();
            
                $i++;
            }
        }
        
        echo '£' . Round($salary / $i);
    }
    
}
