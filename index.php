<?php

/**
 * @author Paul Riley 
 * MGL Test Project
 */

require_once('./model/model.php');
require_once('./view/view.php');
require_once('./controller/controller.php');

$users = new model();
$controller = new controller($users);


if (isset($_REQUEST['action']) && !empty($_REQUEST['action'])) {
    $controller->{$_REQUEST['action']}();
}

$dataView = 'jsonView';
if (isset($_REQUEST['dataView']) && !empty($_REQUEST['dataView'])) {
   $dataView = $_REQUEST['dataView'];
}
$rawDataTable = new $dataView();
$rawDataDisplay =  $rawDataTable->render($users->getRawData());

//Dont render views if this is ajax
if (isset($_REQUEST['ajax']) && !empty($_REQUEST['ajax'])) {
    
    if (isset($_REQUEST['dataView']) && !empty($_REQUEST['dataView'])) {
        echo $rawDataDisplay;
    }
    die();
}




require_once('templates/pageHeader.html');

// Display the data table
require_once('templates/dataDisplayHeader.html');
echo $rawDataDisplay;
require_once('templates/dataDisplayFooter.html');



// Display the age and salary button
require_once('templates/ageSalaryDisplay.html');

// Display the main table header
$tableHeader = new htmlView('templates/tableHeader.html');
 echo $tableHeader->render(array('caheDisplayTime' => user::$cacheTimeString));


foreach ($users->getData() as $user) {

    echo renderTableRow($user);
    
}

require_once('templates/tableFooter.html');
require_once('templates/pageFooter.html');

function renderTableRow($user){
      
    $tableRow = new htmlView($user->getTemplate());
    return $tableRow->render(get_object_vars($user));
}
?>

