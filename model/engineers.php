<?php

/**
 * @author Paul Riley 
 * MGL Test Project
 */

/**
 * model for engineers
 */

class engineers extends user {


    function __construct ($user) {
        parent::__construct($user);
        
        // Engineer properties
        $this->qualifications = $user->qualifications;
        $this->depot = $user->depot;
        $this->field =  $user->field;
        $this->level = $user->level;
        $this->salary =  $user->salary;
        $this->payrollID = $user->payrollID;

    
            $this->template = 'templates/engineersDisplay.html';
 
    }
    
    function getTemplate() {
        return $this->template;

    }
    
    function getSalary(){
        return $this->salary;
    }
    
}
