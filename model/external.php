<?php

/**
 * @author Paul Riley 
 * MGL Test Project
 */

/**
 * model for external users
 */

class external extends user {

    function __construct ($user) {
        parent::__construct($user);
        // External properties
        $this->ipAddress = $user->ipAddress;
        $this->company = $user->company;
        $this->jobTitle = $user->jobTitle;
        
        $this->template = 'templates/externalDisplay.html';
 
    }
    
    function getTemplate() {
        return $this->template;

    }

    
}

