<?php

/**
 * @author Paul Riley 
 * MGL Test Project
 */

/**
 * model for internal users
 */

class internal extends user {


    function __construct ($user) {
        parent::__construct($user);
        // internal properties
        $this->jobTitle = $user->jobTitle;
        $this->salary = $user->salary;
        $this->location = $user->location;
        $this->payrollID =  $user->payrollID;

        $this->template = 'templates/internalDisplay.html';
 
    }
    
    function getTemplate() {
        return $this->template;

    }
    
    function getSalary(){
        return $this->salary;
    }  
    
}

