<?php

/**
 * @author Paul Riley 
 * MGL Test Project
 */

/**
 * model container
 */

require_once('./model/user.php');


class model {
    
    public $data;
    public $rawData;
    
    function __construct (){
        $this->data = user::getUserData();
        $this->rawData = user::$rawData;
    }
    
    function getData(){
        return $this->data;
    }
    
    function getRawData(){
        return $this->rawData;
    }
    
    function refreshData(){
      
         $this->data = user::getUserData(true);
   }   
}
