<?php

/**
 * @author Paul Riley 
 * MGL Test Project
 */

/**
 * model for users
 */

require_once('engineers.php');
require_once('internal.php');
require_once('external.php');

class user {
    
    public static $cacheTimeString;
    public static $rawData;
    
    function __construct($user) {
        
        // Properties common to all users
        $this->firstName = $user->firstName;
        $this->lastName =  $user->lastName;
        $this->DOB = $user->DOB;
        $this->email = $user->email;
        $this->password = $user->password;
        
        $this->template = 'templates/userDisplay.html';
 
    }
    
    function getTemplate() {
        return $this->template;

    }
    
    function getSalary(){
        return null;
    }
    
    function getAge(){
        
        $age = DateTime::createFromFormat('d-m-y', $this->DOB)? DateTime::createFromFormat('d-m-y', $this->DOB) :DateTime::createFromFormat('d-m-Y', $this->DOB);
        $age->diff(new DateTime('now'));
        
        return $age->diff(new DateTime('now'))->y     * 31556926 
                + $age->diff(new DateTime('now'))->m  * 2629743
                + $age->diff(new DateTime('now'))->d  * 6400
                + $age->diff(new DateTime('now'))->h  * 3600
                + $age->diff(new DateTime('now'))->i  * 60
                + $age->diff(new DateTime('now'))->s;

    }
    
    
    public static function getUserData($refresh=false)
    {
        $cache = './data/cache.json';
        $url = 'http://www.mgf.ltd.uk/software-test/api.php';
        $data = array('mgf'=>'userData', 'apiKey'=>'123455678qwertyui');


        $options = array(
                'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        
        //Try and get data from cache
        $result = (file_exists($cache) && !$refresh) ? file_get_contents($cache):FALSE;
      
        if ($result === FALSE) {
            //get file from api
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            if ($result === FALSE) { /* No data !! Handle error */ }
            else { // save cached file
              
                file_put_contents($cache,$result);
            }
        }
        
        clearstatcache();
        static::$cacheTimeString = date("d F Y H:i:s.",filemtime($cache));
        
        static::$rawData = $result;
        
    
        
        $result= json_decode($result);
 
        $i=0;
        foreach ($result as $type => $jsonUsers) {
            
            foreach ($jsonUsers as $jsonUser) {
                
                // build object
                $instance[$i] = new $type($jsonUser);
            
                $i++;

         

            } 
        }
        
        return $instance;
               
    }
    
   
}
