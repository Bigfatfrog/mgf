<?php

/**
 * @author Paul Riley 
 * MGL Test Project
 */

/**
 * View for csv json
 */

class csvView extends view {

  
    protected $data;

    public function __construct() {
   
    }


    /**
     * Render the template, returning it's content.
     * @param array $data Data made available to the view.
     * @return string The rendered template.
     */
    
    public function render( $data) {

        $csv = str_replace('<br/>,','<br>',$this->arrayToCsv(json_decode($data, true))); //quick and dirt display correction
        
        ob_start();
           
        echo '<div>' . $csv  . '</div>';
        $content = ob_get_contents();
       
        ob_end_clean();
        return $content;
    }
    
   
     /**
     * convert json to csv
     * @param array $data Data made available to the view.
     * @return CSV string 
     */
    
    function arrayToCSV($data) {
       
        $csv = array();
        foreach ($data as $item) {
            if (is_array($item)) {
                $csv[] = $this->arrayToCSV($item) ;
            } else {
                $csv[] = $item;
            }
        }
        
        $display = implode(',', $csv) ;
        return rtrim(implode(',', $csv) , '</br>') . '<br/>';
    }
    
}

?>
