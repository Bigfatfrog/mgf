<?php

/**
 * @author Paul Riley 
 * MGL Test Project
 */

/**
 * View for all html templates
 */

class htmlView extends view {
    /**
     * Template being rendered.
     */
    protected $template = null;
    protected $data;


    /**
     * Initialize a new view context.
     */
    public function __construct($template) {
        $this->template = $template;
    }


    /**
     * Render the template, returning it's content.
     * @param array $data Data made available to the view.
     * @return string The rendered template.
     */
    public function render( $data) {
        extract($data);

        ob_start();
        include( $this->template);

        $content = ob_get_contents();
       
        ob_end_clean();
        return $content;
    }
}

?>
