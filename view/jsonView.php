<?php

/**
 * @author Paul Riley 
 * MGL Test Project
 */

/**
 * View for raw json
 */

class jsonView extends view {

  
    protected $data;

    public function __construct() {
   
    }


    /**
     * Render the data, returning it's content.
     */
    public function render( $data) {
   
        ob_start();
       
        echo  ' <div>' .  $data . '</div>';
        $content = ob_get_contents();
       
        ob_end_clean();
        return $content;
    }
}

?>
