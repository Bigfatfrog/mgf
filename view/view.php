<?php

/**
 * @author Paul Riley 
 * MGL Test Project
 */

require_once('htmlView.php');
require_once('jsonView.php');
require_once('xmlView.php');
require_once('csvView.php');

/**
 * View-specific wrapper.
 * Limits the accessible scope available to templates.
 */
class View{
  
    // stuff common to all child views
}

?>
