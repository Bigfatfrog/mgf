<?php

/**
 * @author Paul Riley 
 * MGL Test Project
 */

/**
 * View for xml json
 */

class xmlView extends view {

  
    protected $data;


    /**
     * Initialize a new view context.
     */
    public function __construct() {
   
    }


    /**
     * Render the template, returning it's content.
     * @param array $data Data made available to the view.
     * @return string The rendered template.
     */
    public function render( $data) {

         $xml = new SimpleXMLElement('<root/>');
         $this->arrayToXml(json_decode($data, true), $xml);

        ob_start();
    
       
        echo '  <div>' . htmlspecialchars($xml->asXML()) . '</div>';
        $content = ob_get_contents();
       
        ob_end_clean();
        return $content;
    }
    
   
    
    /**
    * Convert an array to XML
    * @param array $array
    * @param SimpleXMLElement $xml - NB passed by reference!!
    */
   function arrayToXml($array, &$xml){
    
  
       foreach ($array as $key => $value) {
          
                  
           if(is_array($value)){
               if(is_int($key)){
                   $key = "e";
               }
               $label = $xml->addChild($key);
               $this->arrayToXml($value, $label);
           }
           else {
               $xml->addChild($key, $value);
           }
       }
   }
    
}

?>
